<?php

namespace App\Http\Controllers;

use Request;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Http\Resources\TweetResource;


class AivoController extends Controller
{
    public function index()
    {
        $consumerKey = 'Z1WHUJKS1kJJaeWYr1hj8dYu5';
        $consumerSecret = 'NQUPU6U7CAjF8K4Dwk0qJ7F5TIHco6gprsDU27HkJp3q4Gr8Wh';

        $access_token = '1705728343-4czHLVAqPlmcfjFLXmAofPrZVoojWPpGduRhROk';
        $access_token_secret = 'PZqnYdcudEQNKU6OjfHVuCv430SBdGeIV9otBoiWWwXIN';

        $connection = new TwitterOAuth($consumerKey,$consumerSecret , $access_token, $access_token_secret);

        $twetts = $connection->get('statuses/user_timeline',['count'=>10]);

        if(is_object($twetts)){
            return response()->json([
                'message' => 'Something went wrong',
            ], 404);
        }

        $prueba = collect([]);

        foreach ($twetts as $t) {
            $prueba->push($t);
        }

        if ($prueba->isEmpty()) {

            return response()->json([
                'message' => 'Tweets not found',
            ], 404);

        } else {

            return TweetResource::collection($prueba);

        }
    }

    public function aivoheader()
    {

            $consumerKey = Request::header('consumer_key');
            $consumerSecret = Request::header('consumer_secret');
            $accessToken = Request::header('acces_token');
            $accessTokenSecret = Request::header('access_token_secret');

            $connection = new TwitterOAuth($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);

            $statuses = $connection->get("statuses/user_timeline",['count'=>10]);

            //Si falta algun token o esta erroneo devuelve un objeto
            if(is_object($statuses)){
                return response()->json([
                        'message' => 'Something went wrong',
                    ], 404);
            }
            //Hago una colleccion para mandarla al resource
            $colleccionDeTwetts = collect([]);

            foreach ($statuses as $t) {
                $colleccionDeTwetts->push($t);
            }

            if ($colleccionDeTwetts->isEmpty()) {

                return response()->json([
                    'message' => 'Tweets not found',
                ], 404);

            } else {

                return TweetResource::collection($colleccionDeTwetts);

        }

    }
}
