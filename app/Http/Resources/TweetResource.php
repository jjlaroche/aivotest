<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TweetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'created_at'=>$this->created_at,
            'text'=>$this->text,
            $this->mergeWhen($this->in_reply_to_status_id !=null,[
                'in_reply'=>[
                    'id'=>$this->in_reply_to_user_id,
                    'name'=>$this->in_reply_to_screen_name,
                    ]])

        ];
    }
}
